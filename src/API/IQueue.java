package API;

public interface IQueue<Character> {
	

	/**
	 * Agrega un item en la �ltima posici�n de la cola
	 */
	public void  enqueue (Character item) ;
	/**
	 * Elimina el elemento en la primera posici�n de la cola
	 */
	public  Character dequeue () ;
	
	/**
	 * Indica si la cola est� vac�a
	 */
	public  boolean isEmpty(); 
	/**
	 * N�mero de elementos en la cola
	 */
	public int size () ;
}


