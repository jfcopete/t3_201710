package API;

public interface IStack<Character>{
	
	/**
	 * Agrega un item al tope de la pila
	 */
	public void push (Character item);
	
	/**
	 * Elimina el elemento en el tope de la pila
	 */
	public Character pop ();
	
	/**
	 * Indica si la pila est� vac�a
	 */
	public boolean  isEmpty();
	
	/**
	 * N�mero de elementos en la pila
	 */
	public int size();
}		
