package Controller;
import Modelo.*;
import model.data_structures.Queue;
public class controller {
	
	private static Expresion model = new Expresion();
	
	public static boolean ExpresionBienFormada(String expresion) {
		return model.expresionBienFormada(expresion);
	}
	
	public static String OrdenarPila(String expresion) {
		return model.apilar(expresion);
	}
}
