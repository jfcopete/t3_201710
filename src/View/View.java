package View;

import java.util.Scanner;

import Controller.controller;

public class View {
	private static void printMenu(){
		System.out.println("1. Para ver si una expresion est� bien formada");
		System.out.println("2. Para introducir una expresion y ordenarla");
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		printMenu();
		Scanner sc = new Scanner(System.in);
		int option = sc.nextInt();
		switch (option) {
		case 1:
			System.out.println("Ingrese Expresion de b�squeda:");
			String busqueda=sc.next();
			boolean wellFormed = controller.ExpresionBienFormada(busqueda);
			System.out.println("Su expresi�n est� bien formada?"+wellFormed);
			break;
		case 2:
			System.out.println("Ingrese Expresion para ordenar:");
			String ordenar=sc.next();
			System.out.println("Your expresion is "+controller.OrdenarPila(ordenar)+"\n");
			break;
		default:
			break;
		}
	}

}
