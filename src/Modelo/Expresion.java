package Modelo;

import model.data_structures.Queue;
import model.data_structures.Stack;

public class Expresion {

	private Stack<Character> expre;
	private Queue<Character> ordenar;
	private Queue<Character> aux;
	private final static char CADENA1='[';
	private final static char CADENA2='(';
	private final static char CADENA3=')';
	private final static char CADENA4=']';
	private final static char SUMA='+';
	private final static char RESTA='-';
	private final static char DIVISION='/';
	private final static char MULTIPLICACION='*';

	private int llavesAbiertas;
	private int parentesisAbiertos;
	private int parentesisCerrados;
	private int llavesCerrados;
	public Expresion() {
		expre = new Stack<>();
		ordenar= new Queue<>();
		aux= new Queue<>();
		llavesAbiertas=0;
		parentesisAbiertos=0;
		parentesisCerrados=0;
		llavesCerrados =0;
	}
	
	public boolean expresionBienFormada(String expresion) {
		char a [] = expresion.toCharArray();
		for (int i = 0; i < a.length; i++) {
			expre.push(a[i]);
		}
		while (expre.isEmpty()!=true) {
			char compara = expre.pop();
			if (compara==CADENA1) llavesAbiertas++;
			if (compara==CADENA2) parentesisAbiertos++;
			if (compara==CADENA3) parentesisCerrados++;
			if (compara==CADENA4) llavesCerrados++;
			if (expresion.startsWith("[")&&expresion.endsWith("]")&&parentesisAbiertos==parentesisCerrados)return true;
			if (expresion.startsWith("(")&&expresion.endsWith(""))return true;
			if (parentesisAbiertos==parentesisCerrados&&llavesAbiertas==llavesCerrados)return true;
		}
			
		return false;
	}
	
	/*
	public Queue<Character> ordenarPila(String expresion) {
		char [] a = expresion.toCharArray();
		for (int i = 0; i < a.length; i++) {
			ordenar.enqueue(a[i]);
		}while (ordenar.isEmpty()!=true) {
			char comparar = ordenar.dequeue();
			if (comparar==CADENA4||comparar==CADENA3||comparar==CADENA2||comparar==CADENA1) {
				aux.enqueue(comparar);
			}
		}
		while (ordenar.isEmpty()!=true) {
			char comparar = ordenar.dequeue();
			if (comparar==SUMA||comparar==RESTA||comparar==DIVISION||comparar==MULTIPLICACION) {
				aux.enqueue(comparar);
			}
		}
		while (ordenar.isEmpty()!=true) {
			char comparar = ordenar.dequeue();
			boolean compara = Character.isDigit(comparar);
			if (compara) {
				aux.enqueue(comparar);
			}
		}
		
		return aux;
	}
	*/
	
	public String apilar(String expresion){

		String cadena= "";
		if(expresionBienFormada(expresion)){

			char[] split = expresion.toCharArray();

			Stack<Character> stack = new Stack<Character>();		
			for(int i =0; i < split.length;i++){
				stack.push(split[split.length-1-i]);
			}

			stack = ordenarPila(stack);
			for(int i =0; i < split.length;i++){
				cadena = cadena +stack.pop();
			}


		}else{cadena = "La expresi�n ingresada no est� bien formada, por lo cual no se orden�";}

		return cadena;
	}

	public Stack<Character> ordenarPila(Stack<Character> pPila){

		String llaves ="";
		String operadores="";
		String operandos="";
		int tamanio = pPila.size();

		for(int i =0; i <tamanio; i++){

			char aux = pPila.pop();

			if(aux == '['||aux =='('||aux ==')'||aux ==']'){
				 llaves += aux;
			}
			else if(aux =='*'||aux =='+'||aux =='-'){
			      operadores+= aux;

			}else{
				operandos += aux;
			}

		}

		Queue<Character> queue = new Queue<Character>();
		
		char[] llavesS = llaves.toCharArray();
		char[] operadoresS = operadores.toCharArray();
		char[] operandosS = operandos.toCharArray();
		if(llaves != null && operadores != null && operandos != null)
		{
			for(int i =0; i< operandosS.length;i++){
				queue.enqueue(operandosS[i]);
			}
			
			for(int i=0; i < operadoresS.length; i++){
				queue.enqueue(operadoresS[i]);
			}
			
			for(int i =0; i < llavesS.length;i++){
				queue.enqueue(llavesS[i]);
			}

			for(int i = 0; i <tamanio;i++){
				pPila.push(queue.dequeue());
			}

		}
		return pPila;
	}
	
	
	
}
