package model.data_structures;

public class NodoSimple<Character>
{
	private Character item;

	private NodoSimple<Character> next;

	public NodoSimple(Character item)
	{
		this.item = item;
		next = null;
	}


	public Character getItem() 
	{
		return item;
	}

	public void setItem(Character item) 
	{
		this.item = item;
	}

	public NodoSimple<Character> getNext() 
	{
		return next;
	}

	public void setNext(NodoSimple<Character> next) {
		this.next = next;
	}

	public void setNodoSimple(NodoSimple<Character> newNode) 
	{
		next = newNode;
	}
}
