package model.data_structures;

import java.util.Iterator;


public class ListaEncadenada<T> implements ILista<T> {

	private NodoSimple<T> primero;
	private NodoSimple<T> actual;
	private int listSize;
	
	public ListaEncadenada(T item)
	{
		primero = new NodoSimple<T>(item);
		actual = primero;
	}
	
	public int size()
	{
		return listSize;
	}
	
	@Override
	public Iterator<T> iterator() 
	{
		return new Iterator<T>() 
			{
			private NodoSimple<T> actualT=null;
	
			public boolean hasNext() 
			{
				if(actualT==null)return primero.getItem()!=null;
				else return actualT.getNext() != null;
			}
	
			public T next() 
			{
				if(actualT==null)
				{
					actualT=primero;
					if(actualT==null)return null;
					else return actualT.getItem();
				}
				else
				{
					actualT = actualT.getNext();
					return actualT.getItem();
				}
	
			}
		};
}

	@SuppressWarnings("null")
	@Override
	public void agregarElementoFinal(T elem) 
	{
		NodoSimple<T> newNode = new NodoSimple<T>(elem);
		if(primero == null)
		{
			primero=newNode;
		}
		else
		{
			NodoSimple<T> act = primero;
			while(act != null)
			{
					actual = actual.getNext();
			}
			act.setNext(newNode);
		}
		listSize++;
	}

	@Override
	public T darElemento(int pos) 
	{
		actual = primero;
		int contador = 0;
		while(contador<pos)
		{
			actual = actual.getNext();
			contador++;
		}
		return (T) actual.getItem();
	}

	@Override
	public int darNumeroElementos() 
	{
		int contador = 0;
		NodoSimple<T> act = primero;
		while(act != null)
		{
			contador ++;
			act = act.getNext();
		}
		return contador;
	}

	@Override
	public T darElementoPosicionActual() 
	{
		if(actual!=null)
		{
			return actual.getItem();
		}
		else
			return null;
	}

	@Override
	public boolean avanzarSiguientePosicion() 
	{
		if(actual != null && actual.getNext() != null)
		{
			actual = actual.getNext();
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public boolean retrocederPosicionAnterior() 
	{
		NodoSimple<T> act = primero;
		while(act != null)
		{
			if (act.getNext() != null && act.getNext().equals(actual))
			{
				actual = act;
				return true;
			}
			act = act.getNext();
		}
		return false;
	}

}
