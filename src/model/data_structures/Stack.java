package model.data_structures;

import java.util.EmptyStackException;

import API.IStack;

public class Stack<Character> implements IStack<Character>{

	private NodoSimple<Character> topStack;
	private int size;

	public Stack() {
		topStack=null;
		size=0;
	}

	@Override
	public void push(Character item) {
		NodoSimple<Character> newNode = new NodoSimple<>(item);
		if (topStack==null) {
			topStack=newNode;
		}else {
			newNode.setNext(topStack);
			topStack=newNode;
		}
		size++;
	}
	@Override
	public Character pop() {
		if (topStack==null) {
			throw new EmptyStackException();
		}
		Character elem = topStack.getItem();
		NodoSimple<Character> nextTop = topStack.getNext();
		topStack.setNext(null);
		topStack=nextTop;
		size--;
		return  elem;
	}
	@Override
	public boolean isEmpty() {
		if (topStack==null) {
			return true;
		}
		return false;
	}
	@Override
	public int size() {
		return size;
	}

}
