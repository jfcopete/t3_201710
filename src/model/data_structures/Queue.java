package model.data_structures;

import java.util.EmptyStackException;

import API.IQueue;

public class Queue<Character> implements IQueue<Character> {

	private NodoSimple<Character> first;
	private NodoSimple<Character> last;
	private int size;

	public  Queue() {
		size=0;

	}

	@Override
	public void enqueue(Character item) {
		NodoSimple<Character> newNode = new NodoSimple<>(item);
		if (size==0) {
			first = newNode;
			last = newNode;
		}else {
			NodoSimple<Character> oldLast=last;
			oldLast.setNext(newNode);
			last=newNode;
		}
		size++;
	}

	@Override
	public Character dequeue() {
		Character item  = first.getItem();
			first = first.getNext();
			if(isEmpty()) last = null;
			size--;
			return item;
		
	}

	@Override
	public boolean isEmpty() {
		if (size==0) {
			return true;
		}
		return false;
	}

	@Override
	public int size() {
		return size;
	}


}
